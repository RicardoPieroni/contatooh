var http = require('http');
var app = require('./config/express.js')(); //por seu retorno ser uma função, usamos () para executá-la

/*função http.createServer recebe como parâmetro um request
listener. Em nosso caso, é a instância do Express que será aplicada ao
evento request disparada em cada requisição. */

http.createServer(app).listen(app.get('port'), function(){
	console.log('Express Server escutando na porta ' +
	app.get('port'));
});