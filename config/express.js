//A variável express armazena uma função que, ao ser chamada, retorna
//uma instância do Express:
var express = require('express');

var home = require('../app/routes/home')

//indicam que o módulo depende do Express.
module.exports = function(){
	var app = express();


//Variáveis de ambiente são usadas para inicializar configurações padrões do Express.
app.set('port',3000);

// middleware
app.use(express.static('./public'));

/*Express suporta uma grande variedade de templates engines. Utilizaremos
o engine EJS (http://embeddedjs.com) que possui uma sintaxe similar ao
HTML, facilitando sua aceitação entre designers.*/
// abaixo do middleware express.static
app.set('view engine', 'ejs');
app.set('views','./app/views');

home(app);

	return app;
}

